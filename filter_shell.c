#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define CMD_SYSCTL          "sysctl"
#define CMD_GET_ROUTE       "cat /proc/net/route"
#define CMD_GET_ALT_DNS     "cat /etc/resolv.conf 2>&- |awk '{if ($1==\"nameserver\") {print $2}}' |awk 'NR>1'"
#define CMD_GET_DNS         "cat /etc/resolv.conf 2>&- |awk '{if ($1==\"nameserver\") {print $2;exit}}"
#define CMD_DHCLIENT_PID    "pidof dhclient"
#define CMD_DHCLIENT        "dhclient "
#define CMD_GETCONF         "getconf "

#define COL         "\033[0m"
#define COL_BRED    "\033[1;31m"
#define COL_BGREEN  "\033[1;32m"
#define COL_BYELLOW "\033[1;33m"
#define COL_BBLUE   "\033[1;34m"
#define COL_BPURPLE "\033[1;35m"
#define COL_BCYAN   "\033[1;36m"
#define COL_BWHITE  "\033[1;37m"

typedef struct pinfo {
    FILE         *file;
    pid_t         pid;
    struct pinfo *next;
} pinfo;

static pinfo *plist = NULL;

int fork();
int close(int fd);
int fileno(FILE *fp);
int pipe(int fildes[2]);
int execl(const char* path, const char* arg, ...);
FILE *fdopen(int fd, const char *mode);

int system(const char* command)
{
    int pid;
    int status;

    if (!strncmp(CMD_SYSCTL, command, strlen(CMD_SYSCTL))) {
        printf(COL_BCYAN "[=]" COL " %s\n", command);
        return 0;
    }
    printf(COL_BGREEN "[>]" COL " %s\n", command);

    if ((pid = fork()) == -1) {
        perror("fork");
        return -1;
    }

    if (pid == 0) { // child
        if (execl("/bin/sh", "sh", "-c", command, NULL)) {
            perror("execl");
            return -1;
        }
    }

    waitpid(pid, &status, 0);
    return WEXITSTATUS(status);
}

// Feed content into FILE stream
FILE* fakefile(const char* content)
{
    FILE *tmpfp;

    tmpfp = tmpfile();
    fprintf(tmpfp, "%s", content);
    return tmpfp;
}

pinfo *fakepopen(const char *content)
{
    pinfo *cur;

    cur = (pinfo *) malloc(sizeof(pinfo));
    if (!cur) {
        return NULL;
    }
    // Here we use a special pid to identify fake pinfo
    cur->pid = -2;
    cur->file = fakefile(content);
    cur->next = plist;
    plist = cur;
    return cur;
}

// from http://cnds.eecs.jacobs-university.de/courses/os-2011/src/popen/popen.c
FILE* popen(const char *command, const char *mode)
{
    int fd[2];
    pinfo *cur, *old;

    if (!strncmp(CMD_GETCONF, command, strlen(CMD_GETCONF))) {
        // noop, just to disable output
    } else if (!strncmp(CMD_GET_DNS, command, strlen(CMD_GET_DNS))) {
        printf(COL_BRED "[F]" COL " %s\n", command);

        if (!(cur = fakepopen("nameserver 202.114.0.242\nnameserver 202.112.20.131\n"))) {
            errno = ENOMEM;
            return NULL;
        }

        return cur->file;
    } else if (!strncmp(CMD_DHCLIENT, command, strlen(CMD_DHCLIENT))) {
        // what to do here?
    } else {
        // record those silly popen
        printf(COL_BGREEN "[%s]" COL " %s\n", mode, command);
    }

    if (mode[0] != 'r' && mode[0] != 'w') {
        errno = EINVAL;
        return NULL;
    }

    if (mode[1] != 0) {
        errno = EINVAL;
        return NULL;
    }

    if (pipe(fd)) {
        return NULL;
    }

    cur = (pinfo *) malloc(sizeof(pinfo));
    if (! cur) {
        errno = ENOMEM;
        return NULL;
    }

    cur->pid = fork();
    switch (cur->pid) {

    case -1:                    /* fork() failed */
        close(fd[0]);
        close(fd[1]);
        free(cur);
        return NULL;

    case 0:                     /* child */
        for (old = plist; old; old = old->next) {
            close(fileno(old->file));
        }

        if (mode[0] == 'r') {
            dup2(fd[1], STDOUT_FILENO);
        } else {
            dup2(fd[0], STDIN_FILENO);
        }
        close(fd[0]);   /* close other pipe fds */
        close(fd[1]);

        execl("/bin/sh", "sh", "-c", command, (char *) NULL);
        _exit(1);

    default:                    /* parent */
        if (mode[0] == 'r') {
            close(fd[1]);
            if (!(cur->file = fdopen(fd[0], mode))) {
                close(fd[0]);
            }
        } else {
            close(fd[0]);
            if (!(cur->file = fdopen(fd[1], mode))) {
                close(fd[1]);
            }
        }
        cur->next = plist;
        plist = cur;
    }

    return cur->file;
}

int pclose(FILE *file)
{
    pinfo *last, *cur;
    int status;
    pid_t pid;

    /* search for an entry in the list of open pipes */

    for (last = NULL, cur = plist; cur; last = cur, cur = cur->next) {
        if (cur->file == file) break;
    }
    if (! cur) {
        fclose(file);
        errno = EINVAL;
        return -1;
    }

    /* remove entry from the list */

    if (last) {
        last->next = cur->next;
    } else {
        plist = cur->next;
    }

    /* wait for process termination */

    fclose(file);
    if (cur->pid == -2) {
        return 0;
    }

    do {
        pid = waitpid(cur->pid, &status, 0);
    } while (pid == -1 && errno == EINTR);

    /* release the entry for the now closed pipe */

    free(cur);

    if (WIFEXITED(status)) {
        return WEXITSTATUS(status);
    }
    errno = ECHILD;
    return -1;
}

