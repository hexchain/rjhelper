all:
	gcc -Wall -shared -fPIC -o filter_shell.so filter_shell.c

clean:
	rm filter_shell.so
